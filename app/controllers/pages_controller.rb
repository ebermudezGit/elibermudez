class PagesController < ApplicationController
  def index
  end
  
  def landing
    render layout: "layout2"
  end

  def save_forma
  	if params[:nombre].blank?
  		redirect_to pages_index_path,notice: "Falta llenar el nombre"
  	elsif params[:email].blank?
  		redirect_to pages_index_path,notice: "Falta llenar el email"
  	else
      User.create(nombre: params[:nombre],email: params[:email])
      redirect_to pages_index_path,notice: "El Usuario se Guardo. Nombre:#{params[:nombre]} email:#{params[:email]}"
    end
  end

end
